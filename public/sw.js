//VERSION: 1.0.1
// 1. Save the files to the user's device
// The "install" event is called when the ServiceWorker starts up.
// All ServiceWorker code must be inside events.
self.addEventListener('install', function(e) {
  console.log('install');

  // waitUntil tells the browser that the install event is not finished until we have
  // cached all of our files
  e.waitUntil(
    // Here we call our cache "weddingpwa", but you can name it anything unique
    caches.open('weddingpwa').then(cache => {
      // If the request for any of these resources fails, _none_ of the resources will be
      // added to the cache.
      return cache.addAll([
        '/',
        '/index.html',
        '/manifest.json',
        '/css/onsenui.min.css',
        '/css/onsen-css-components.min.css',
        '/css/ionicons/css/ionicons.min.css',
        '/css/material-design-iconic-font/css/material-design-iconic-font.min.css',
        '/css/font_awesome/css/font-awesome.min.css',
        '/css/font_awesome/fonts/fontawesome-webfont.woff?v=4.7.0',
        '/css/font_awesome/fonts/fontawesome-webfont.woff2?v=4.7.0',
        '/css/font_awesome/webfonts/fa-solid-900.woff2',
        '/css/font_awesome/webfonts/fa-brands-400.woff2',
        '/dist.js',
        '/logo_512.png'
        '/logo_192.png'
      ]);
    })
  );
});

// 2. Intercept requests and return the cached version instead
self.addEventListener('fetch', function(e) {
  e.respondWith(
    // check if this file exists in the cache
    caches.match(e.request)
      // Return the cached file, or else try to get it from the server
      .then(response => response || fetch(e.request))
  );
});
