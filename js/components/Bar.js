import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Card,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  ListTitle,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';


class Bar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.bar = [
          ["Draft Beers, Ales & Ciders"],
          ["Korev Lager", "5%"],
          ["Sulis Lager","4.4%"],
          ["Gem Bitter","4%"],
          ["Thatchers Cider","4.8%"],
          ["Bottled Beers, Ales & Ciders"],
          ["Corona Extra", "330ml 4.6%"],
          ["Bath Ales","500ml 4.8%"],
          ["Tribute Pale Ale","500ml 4.2%"],
          ["Old Mout and Brothers Cider","500ml 4.0%"],
          ["White Wine"],
          ["Alma de Vid Blanco", "11%"],
          ["Melodias Pinot Grigio", "12.50%"],
          ["Red Wine"],
          ["Alma de Vid Tinto", "12%"],
          ["Carmenere Reserva", "13%"],
          ["Rose Wine"],
          ["La Lande Cinsualt Rose", "12%"],
          ["Soft Drinks"],
          ["San Pellegrino", "330ml"],
          ["Coke","Small / Large"],
          ["Diet Coke","Small / Large"],
          ["Lemonade","Small / Large"],
          ["Ginger Ale",""],
          ["Bitter Lemon",""],
          ["Elderflower Presse",""],
          ["Fruit Juice","Small / Large"],
        ]
    };
    goBack = () => {
      this.props.navigator.popPage();
    }
    renderToolbar = () => {
        return (
            <Toolbar>
                <BackButton onClick={this.goBack}></BackButton>
                <div className='center'>Bar Selection</div>
            </Toolbar>
        );
    }

    renderRow = (row, index) => {
      if( row.length == 1){
        return (
          <ListTitle
            key={"Bar-"+index}
            modifier=""
          >
              {row[0]}
          </ListTitle>
        );

      }
      else{
        return (
          <ListItem
            key={"Bar-"+index}
            modifier=""
          >
            <div className='center'>
              {row[0]}
            </div>
            <div className='right'>
              <div className='list-item__label'>
                {row[1]}
              </div>
            </div>
          </ListItem>
        );

      }
    };

    render() {
      return (
        <Page key={"Bar"}
            renderToolbar={this.renderToolbar}
      >
        <List
            dataSource={this.bar}
            renderRow={this.renderRow}
          />
      </Page>
      );
    }
};

module.exports = Bar;
