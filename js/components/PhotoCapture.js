import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Checkbox,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';


class PhotoCapture extends React.Component {
  constructor(props) {
    super(props);
    this.photo_categories = [
      [
        "With the married couple"
      ],
      // With another guest at the wedding
      [
        "With Emiliano",
        "With Leo",
        "With Kerry",
        "With Jimmy",
        "With Ngaiman",
        "With George",
        "With Penny",
        "With Nat",
        "With Dan",
        "With Joyce",
        "With Alan",
        "With Matt",
        "With Vit",
        "With Jim",
        "With Alex",
        "With Nash",
        "With Jacek",
        "With Mike",
      ],
      // With challenge / nice photo
      [
        "With a Zoo visitor",
        "With a Zoo staff",
        "With the wedding photographer",
        "With the witnesses",
        "Photo of someone taking a photo of someone taking a photo of someone taking a selfie",
        "A reference to a movie",
        "A where is waldo style",
      ],
      //With an animal
      [
        "With a bird",
        "With a lion",
        "With a monkey",
        "With a penguin",
        "With an insect",
        "With a red panda",
        "With flamingo",
        "With a meerkat",
        "With a turtle",
      ],
      //Nice photo
      [
        "Happy black and white photo",
        "Macro",
        "Artistic blurry",
        "In motion",
        "Cool shadows",
        "Top view of shoes",
        "Symetrical",
        "Person's Back",
        "Portrait (blurry background)",
      ]
    ]
    const save_string = localStorage.getItem('user_photos');
    // const save_string = ""

    if (save_string) {
      const save = JSON.parse(save_string);
      this.state = save;
    }
    else {
      this.state = {
        selections: this.photo_categories.map((photos) => {
          return Math.floor(Math.random() * photos.length)
        }),
        checked: this.photo_categories.map((photos) => {
          return false
        })
      };
    }
  };
  goBack = () => {
    this.props.navigator.popPage();
  }
  renderToolbar = () => {
    return (
      <Toolbar>
        <BackButton onClick={this.goBack}></BackButton>
        <div className='center'>Photo Challenge</div>
      </Toolbar>
    );
  }

  handleClick = () => {
    window.location = 'googlephotos://';
  };

  renderFixed = () => {
    return (
      <a href="https://photos.app.goo.gl/dHZbF8sKCHTUDrmm6" target="_blank">
        <Fab
          // onClick={this.handleClick}
          position='bottom right'>
          <Icon icon='fa-camera' />
        </Fab>
      </a>
    );
  };

  handleCheck = (index) => {
    this.setState({
      checked: this.state.checked.map((item, j) => {
        return index === j ? !item : item
      })
    })
  }

  renderRow = (row, index) => {
    return (
      <ListItem
        key={"photo-" + index}
        modifier="longdivider material"
      >

        <label className='left'>
          <Checkbox
            inputId={"radio-" + index}
            onChange={this.handleCheck.bind(this, index)}
            modifier='material'
            checked={row['checked']}
          />
        </label>
        <label htmlFor={"radio-" + index} className='center'>
          <span className="list-item__title">{row['name']}</span>
        </label>
      </ListItem>
    );
  };

  render() {

    localStorage.setItem('user_photos', JSON.stringify(this.state));
    let bottom = null;
    let data_source = this.photo_categories.map((photos, i) => {
      return {
        'name': photos[this.state.selections[i]],
        'checked': this.state.checked[i],
      }
    })
    let content = <List
      dataSource={data_source}
      renderRow={this.renderRow}
      renderHeader={() => <ListHeader><p>Please take some special pictures (if you want)</p><p>Share them on the wedding google photo album:</p></ListHeader>}
    />;
    return (
      <Page key={"Photos"}
        renderToolbar={this.renderToolbar}
        renderFixed={this.renderFixed}
      >
        {content}
      </Page>
    );
  }
};

module.exports = PhotoCapture;
