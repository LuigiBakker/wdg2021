import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Card,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';


class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  };
  goBack = () => {
    this.props.navigator.popPage();
  }
  renderToolbar = () => {
    return (
      <Toolbar>
        <BackButton onClick={this.goBack}></BackButton>
        <div className='center'>Welcome</div>
      </Toolbar>
    );
  }

  render() {
    return (
      <Page key={"Welcome"}
        renderToolbar={this.renderToolbar}
      >
        <Card>
          <h2>Min & Fran's big day</h2>
          <p>
            Thanks for coming to our wedding.
            Today is our special day and we want you to be a part of it.
          </p>
          <p>
            Few informations about the day: our photographer is named Kitty.
            Don't forget to sign the guest poster and to take a picture with the polaroid.
            Some of the activities will require your participation and will get revealed during the day.
            Tears of joy are allowed and welcome.
          </p>
        </Card>
        <Card>
          <h3>Groom's Family (Absent)</h3>
          <p>Bruno Regnoult</p>
          <p>Marie-Claude Blandin</p>
          <p>Antoine Regnoult</p>
          <p>Emilie Regnoult</p>
          <h3>Bride's Family (Absent)</h3>
          <p>Choo Hwa Lim</p>
          <p>Fan Chi Kung</p>
          <p>Min Yu Lim</p>
          <h3>Best Man</h3>
          <p>Michael Hoy</p>
          <h3>Maid of Honor</h3>
          <p>Nadia Rodriguez</p>
        </Card>
      </Page>
    );
  }
};

module.exports = Welcome;
