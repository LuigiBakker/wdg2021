import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Card,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';


class Schedule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.timeline = [
          ["Guests arrival", "10:45 - 11:15"],
          ["Ceremony", "11:30 - 12:00"],
          ["Bar opening", "12:00 - 17:30"],
          ["Zoo visit", "12:00 - 14:00"],
          ["Married couple Photography shooting", "12:00 - 13:00"],
          ["Wedding breakfast", "14:30 - 15:30"],
          ["Wedding Toast", "15:30 - 15:45"],
          ["Love, music and romance", "15:45 - 17:30"],
          ["Pub retreat", "17:30 - 23:45"],
        ]
    };
    goBack = () => {
      this.props.navigator.popPage();
    }
    renderToolbar = () => {
        return (
            <Toolbar>
                <BackButton onClick={this.goBack}></BackButton>
                <div className='center'>Today's Schedule</div>
            </Toolbar>
        );
    }

    renderRow = (row, index) => {
      return (
        <ListItem
          key={"photo-"+index}
          modifier=""
        >

          <div className='center'>
            {row[0]}
          </div>
          <div className='right'>
            <div className='list-item__label'>
              {row[1]}
            </div>
          </div>
        </ListItem>
      );
    };

    render() {
      return (
        <Page key={"Schedule"}
            renderToolbar={this.renderToolbar}
      >
        <List
            dataSource={this.timeline}
            renderRow={this.renderRow}
          />
      </Page>
      );
      // renderHeader={() => <ListHeader>You've all received a small challenge to take few special pictures. Please share them on the wedding google photo album:</ListHeader>}
    }
};

module.exports = Schedule;
