import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Checkbox,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';

function msToDisplay(millisec) {
    var seconds = (millisec / 1000).toFixed(0);
    var minutes = Math.floor(seconds / 60);
    var hours = "";
    if (minutes > 59) {
        hours = Math.floor(minutes / 60);
        hours = (hours >= 10) ? hours : "0" + hours;
        minutes = minutes - (hours * 60);
        minutes = (minutes >= 10) ? minutes : "0" + minutes;
    }

    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    if (hours != "") {
        return hours + ":" + minutes + ":" + seconds;
    }
    return minutes + ":" + seconds;
}

class TimedListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {now:new Date()};
        setInterval(this.updateDate, 1000);
    };

    updateDate = () => {
      this.setState({now:new Date()})
    }

    render() {
      const that = this;
      let content = null;
      if (this.props.date > this.state.now){
        content = <ListItem>
        <div className='center'>
          Discover at {this.props.date.toLocaleTimeString().slice(0,5)}
        </div>
        <div className='right'>
            {msToDisplay(this.props.date - this.state.now)}
        </div>
        </ListItem>;
      }
      else{
        content = <ListItem
          onClick={() => this.props.navigator.pushPage({comp: that.props.component })}
          >
            <div className='center'>
                {this.props.row['title']}
            </div>
            <div className='right'>
                <Icon icon='fa-chevron-right' />
            </div>
        </ListItem>;
      }
      return (
        <div>
          {content}
        </div>
      );
    }
};

module.exports = TimedListItem;
