import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Card,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  ListTitle,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';


class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.menu = [
          "Starter",
          "Wye Valley smoked mackerel, Celery salad, charred cucumber, yoghurt and dill",
          "Main course",
          "Pan fried British breast of chicken, Dauphinoise potatoes, heritage carrots, sauté kale, chicken jus",
          "Dessert",
          "Elderflower set cream, raspberries, thyme crumble"
        ]
        this.veg_menu = [
          "Starter",
          "Isle of Wight tomato gazpacho, Pesto and sourdough croutons",
          "Main course",
          "Butternut squash risotto, Roasted squash, roast mushroom, parmesan crisp",
          "Dessert",
          "Chocolate courgette cake, Poached berries"
        ]
        this.wine_menu = [
          "Starter",
          "Villemarin Picpoul de Pinet 2020, France",
          "Main course",
          "Louis Latour Mâcon-Lugny 2019/20",
          "Dessert",
          "Château Minuty 'M de Minuty' Rosé 2019, Côtes de Provence"
        ]
    };
    goBack = () => {
      this.props.navigator.popPage();
    }
    renderToolbar = () => {
        return (
            <Toolbar>
                <BackButton onClick={this.goBack}></BackButton>
                <div className='center'>Menus</div>
            </Toolbar>
        );
    }

    renderRow = (row, index) => {
      if(index %2 == 0){
        return (
          <ListTitle
            key={"menu-"+index}
            modifier=""
          >
              {row}
          </ListTitle>
        );

      }
      else{
        return (
          <ListItem
            key={"menu-"+index}
            modifier=""
          >
              {row}
          </ListItem>
        );

      }
    };

    render() {
      return (
        <Page key={"Menu"}
            renderToolbar={this.renderToolbar}
      >
        <List
            dataSource={this.menu}
            renderHeader={() => <ListHeader>Breakfast Menu </ListHeader> }
            renderRow={this.renderRow}
          />
        <List
            dataSource={this.veg_menu}
            renderHeader={() => <ListHeader>Vegan Menu </ListHeader> }
            renderRow={this.renderRow}
          />
        <List
            dataSource={this.wine_menu}
            renderHeader={() => <ListHeader>Wine Menu </ListHeader> }
            renderRow={this.renderRow}
          />
      </Page>
      );
    }
};

module.exports = Menu;
