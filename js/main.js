import React from 'react';
import ReactDOM from 'react-dom';

import ons from 'onsenui';

import {
  ListItem,
  BottomToolbar,
  Button,
  Radio,
  Icon,
  Input,
  Page,
  List,
  ListHeader,
  Tab,
  Toolbar,
  BackButton,
  Segment,
  Modal,
  Tabbar,
  Fab,
  Navigator
} from 'react-onsenui';

import PhotoCapture from './components/PhotoCapture';
import Schedule from './components/Timeline';
import Menu from './components/Menu';
import Welcome from './components/Welcome';
import Bar from './components/Bar';

import TimedListItem from './components/TimedListItem';

class SelectionPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      players: []
    };

    this.pages = [
      {
        title: 'Welcome',
        component: Welcome
      },
      {
        title: 'Menu',
        component: Menu
      },
      {
        title: 'Bar',
        component: Bar
      },
      {
        title: 'Schedule',
        component: Schedule
      },
      {
        title: 'Photos',
        render: (r, i) => {
          return (
            <TimedListItem key={"main-" + i}
              date={new Date(2021, 7, 14, 12, 0, 0)}
              // date={new Date(2021, 6, 18, 17, 18, 0)} # For debugging
              row={r}
              navigator={this.props.navigator}
              component={PhotoCapture}
            />);
        }
      },
      {
        title: 'Donation',
        render: (r, i) => {
          return (
            <ListItem
              key="main-donation"
              onClick={() => window.open("https://paypal.me/pools/c/8BQFMUpaQu", '_blank').focus()}
            >
              <div className='center'>
                Honey moon fund
              </div>
              <div className='right'>
                <Icon icon='fa-chevron-right' />
              </div>
            </ListItem>
          );
        }
      }
    ]
  };

  componentDidMount = () => {
    const save_string = localStorage.getItem('game_save');
    if (save_string) {
      const save = JSON.parse(save_string);
      if (save["turn_count"] > 0) {
        const that = this;
        ons.notification.confirm("Re-Loading ancienne partie?",
          {
            title: '', buttonLabels: ['Annuler', 'Confirmer'], callback: function (choice) {
              if (choice === 1) {
                console.log("Loading previous game")
                that.props.navigator.pushPage({ comp: PhotoCapture, props: {} });

              }
            }
          });
      }
    }
  }

  renderToolbar = () => {
    return (
      <Toolbar>
        <div className='center'>Min & Fran Wedding 14th August 2021</div>
      </Toolbar>
    );
  }

  renderRow = (row, index) => {
    if (row['render'] || false) {
      return row['render'](row, index)
    } else {

      return (
        <ListItem key={"main-" + index}
          onClick={() => this.props.navigator.pushPage({ comp: row['component'], props: row['props'] || {} })}
        >
          <div className='center'>
            {row['title']}
          </div>
          <div className='right'>
            <Icon icon='fa-chevron-right' />
          </div>
        </ListItem>
      );
    }
  };

  render() {
    let bottom = null;
    let content = <List
      dataSource={this.pages}
      renderRow={this.renderRow}
      renderHeader={() => <ListHeader>Menu</ListHeader>}
    />;
    return (
      <Page key={"Home"}
        renderToolbar={this.renderToolbar}
      >
        {content}
      </Page>
    );
  }
};

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  renderPage(route, navigator) {
    route.props = route.props || {};
    route.props.navigator = navigator;
    return React.createElement(route.comp, route.props);
  }

  render() {
    return (
      <Navigator
        initialRoute={{ comp: SelectionPage }}
        renderPage={this.renderPage}
      />
    );
  }
};

ReactDOM.render(<Home />, document.getElementById('app'));
